## Методы программирования 2: Полиномы

### Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

- ввод полинома 


- организация хранения полинома


- удаление введенного ранее полинома;


- копирование полинома;


- сложение двух полиномов;


- вычисление значения полинома при заданных значениях переменных;


- вывод.


В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:


- поддержка понятия текущего звена;


- вставка звеньев в начало, после текущей позиции и в конец списков;


- удаление звеньев в начале и в текущей позиции списков;


- организация последовательного доступа к звеньям списка (итератор).

#### Условия и ограничения

1.	Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.

2.	Степени переменных полиномов не могут превышать значения 9, т.е. 0 <= i, j, k <= 9.

3.	Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

### Выполнение работы


#### Общая структура классов


- класс TDatValue для определения класса объектов-значений списка (абстрактный класс);


- класс TMonom для определения объектов-значений параметров монома;


- класс TRootLink для определения звеньев (элементов) списка (абстрактный класс);


- класс TDatLink для определения звеньев (элементов) списка с указателем на объект-значение;


- класс TDatList для определения линейных списков;


- класс THeadRing для определения циклических списков с заголовком;


- класс TPolinom для определения полиномов.


![](./image/struct.png)


#### Реализация классов

##### TDatValue:

```c++
#ifndef _DATVALUE_H_
#define _DATVALUE_H_

#pragma once

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue 
{
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
#endif
```

##### TMonom:

```c++
#ifndef _MONOM_H_
#define _MONOM_H_

#pragma once
#include "DatValue.h"

class TMonom;
typedef TMonom *PTMonom;

class TMonom : public TDatValue 
{
protected:
	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)
public:
	TMonom(int cval = 1, int ival = 0) 
	{
		Coeff = cval; 
		Index = ival;
	};
	virtual TDatValue * GetCopy() // изготовить копию
	{
		TDatValue * temp = new TMonom(Coeff, Index);
		return temp;
	}; 
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }
	TMonom& operator=(const TMonom &tm) 
	{
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	int operator==(const TMonom &tm) 
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator<(const TMonom &tm) 
	{
		return Index<tm.Index;
	}
	friend class TPolinom;
};
#endif
```

##### TRootLink:

```c++
#ifndef _ROOTLINK_H_
#define _ROOTLINK_H_

#pragma once
#include <iostream>
#include "DatValue.h"
using namespace std;

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink 
{
protected:
	PTRootLink  pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = NULL) { pNext = pN; }
	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink) 
	{
		PTRootLink p = pNext;  pNext = pLink;
		if (pLink != NULL) pLink->pNext = p;
	}
	virtual void       SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};
#endif
```

##### TDatLink:

```c++
#ifndef _DATLINK_H_
#define _DATLINK_H_

#include <iostream>
#include "RootLink.h"
using namespace std;

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink 
{
protected:
	PTDatValue pValue;  // указатель на объект значения
public:
	TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) :
		TRootLink(pN) 
	{
		pValue = pVal;
	}
	void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};
#endif

```

##### TDatList:

```c++
#ifndef _DATLIST_H_
#define _DATLIST_H_

#pragma once
#include <iostream>
#include "DatLink.h"
using namespace std;

enum TLinkPos {FIRST, CURRENT, LAST};

class TDatList
{
protected:
	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка 
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке
protected:  // методы
	PTDatLink GetLink(PTDatValue pVal = NULL, PTDatLink pLink = NULL);
	void      DelLink(PTDatLink pLink);   // удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }
	// доступ
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty()  const { return pFirst == pStop; } // список пуст ?
	int GetListLength()    const { return ListLen; }       // к-во звеньев
														   // навигация
	void SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual void Reset(void);             // установить на начало списка
	virtual bool IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)
										 // вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // перед первым
	virtual void InsLast(PTDatValue pVal = NULL); // вставить последним 
	virtual void InsCurrent(PTDatValue pVal = NULL); // перед текущим 
													 // удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список
};
#endif

#include "DatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

TDatList::TDatList()
{
	pFirst = nullptr;
	pLast = nullptr;
	pStop = nullptr;
	ListLen = 0;
	Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink temp;
	switch (mode)
	{
	case FIRST: temp = pFirst; break;
	case LAST: temp = pLast; break;
	default: temp = pCurrLink;  break;
	}
	return (temp == nullptr) ? nullptr : temp->pValue;
}

void TDatList::SetCurrentPos(int pos)
{
	if (pos < 0 || pos >= GetListLength())
		throw "Incorrect position index";
	Reset();
	for (int i = 0; i < pos; i++)
		GoNext();
}

int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}

void TDatList::Reset(void)
{
	pPrevLink = pStop;
	if (!IsEmpty())
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
	else
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
}

bool TDatList::IsListEnded(void) const
{
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{
	if (!IsListEnded())
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp == nullptr)
		throw "Incorrect DatLink pointer";
	pFirst = temp;
	ListLen++;
	if (ListLen == 1)
	{
		pLast = temp;
		Reset();
	}
	else
		if (CurrPos == 0)
			pCurrLink = temp;
		else
			CurrPos++;
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink temp = GetLink(pVal, pStop);
	if (temp == nullptr)
		throw "Incorrect DatLink pointer";
	if (pLast != nullptr)
		pLast->SetNextLink(temp);
	pLast = temp;
	ListLen++;
	if (ListLen == 1)
	{
		pFirst = temp;
		Reset();
	}
	if (pCurrLink == pStop)
		pCurrLink == temp;
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink temp = GetLink(pVal, pCurrLink);
		if (temp == nullptr)
			throw "Incorrect DatLink pointer";
		pPrevLink->SetNextLink(temp);
		temp->SetNextLink(pCurrLink);
		ListLen++;
		pCurrLink = temp;
	}
}

void TDatList::DelFirst(void)
{
	if (IsEmpty())
		throw "List is empty";
	PTDatLink temp = pFirst;
	pFirst = pFirst->GetNextDatLink();
	DelLink(temp);
	ListLen--;
	if (IsEmpty())
	{
		pLast = pStop;
		Reset();
	}
	else if (CurrPos == 0)
		pCurrLink = pFirst;
	else if (CurrPos == 1)
		pCurrLink == pStop;
	if (CurrPos)
		CurrPos--;
}

void TDatList::DelCurrent(void)
{
	if (pCurrLink != pStop)
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else
		{
			PTDatLink temp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(temp);
			ListLen--;

			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
}
```

##### THeadRing:

```c++
#ifndef _HEADRING_H_
#define _HEADRING_H_

#pragma once
#include "DatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // после заголовка
												   // удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено
};
#endif

#include "HeadRing.h"

THeadRing::THeadRing() : TDatList()
{
	InsLast();
	pHead = pFirst;
	ListLen = 0;
	pStop = pHead;
	Reset();
	pFirst->SetNextLink(pFirst);
}

THeadRing::~THeadRing()
{
	TDatList::~TDatList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void)
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}

```
##### TPolinom:

```c++
#ifndef _POLINOM_H_
#define _POLINOM_H_

#pragma once
#include "HeadRing.h"
#include "Monom.h"

class TPolinom : public THeadRing 
{
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // конструктор
												  // полинома из массива «коэффициент-индекс»
	TPolinom(TPolinom &q);      // конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); };
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator-(TPolinom &q);	//вычитание полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q);	// сравнение
	int Calculate(int x = 0, int y = 0, int z = 0);

	friend ostream & operator<<(ostream & os, TPolinom & q);
};
#endif

#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (int i = 0; i < km; i++)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		monom = q.GetMonom();
		InsLast(monom->GetCopy());
	}
}

TPolinom & TPolinom::operator+(TPolinom & q)
{
	PTMonom qM, thisM, tempM;
	bool flag = true;
	TPolinom* res = new TPolinom();
	q.Reset();
	Reset();
	while (flag)
	{
		qM = q.GetMonom();
		thisM = GetMonom();
		if (qM->Index < thisM->Index)
		{
			res->InsLast(thisM->GetCopy());
			GoNext();
		}
		else if (thisM->Index < qM->Index)
		{
			res->InsLast(qM->GetCopy());
			q.GoNext();
		}
		else if (qM->Index == thisM->Index)
		{
			if (thisM->Index == -1)
				 flag = false;
			tempM = new TMonom(qM->Coeff + thisM->Coeff, thisM->Index);
			if (tempM->Coeff != 0)
				 res->InsLast(tempM->GetCopy());
			GoNext();
			q.GoNext();
		}
	}
	return *res;
}

TPolinom & TPolinom::operator-(TPolinom & q)
{
	PTMonom qM, thisM, tempM;
	bool flag = true;
	TPolinom* res = new TPolinom();
	q.Reset();
	Reset();
	while (flag)
	{
		qM = q.GetMonom();
		thisM = GetMonom();
		if (qM->Index < thisM->Index)
		{			
			res->InsLast(thisM->GetCopy());
			GoNext();
		}
		else if (thisM->Index < qM->Index)
		{
			tempM = new TMonom(-(qM->Coeff), qM->Index);
			res->InsLast(tempM->GetCopy());
			q.GoNext();
		}
		else if (qM->Index == thisM->Index)
		{
			if (thisM->Index == -1)
				flag = false;
			tempM = new TMonom( - qM->Coeff + thisM->Coeff, thisM->Index);
			if (tempM->Coeff != 0)
				res->InsLast(tempM->GetCopy());
			GoNext();
			q.GoNext();
		}
	}
	return *res;
}

TPolinom & TPolinom::operator=(TPolinom & q)
{
	if (&q != nullptr && &q != this)
	{
		DelList();
		PTMonom monom = new TMonom(0, -1);
		pHead->SetDatValue(monom);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			monom = q.GetMonom();
			InsLast(monom->GetCopy());
		}
	}
	return *this;
}

bool TPolinom::operator==(TPolinom & q)
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		Reset(); q.Reset();
		PTMonom thisM, qM;
		while (!IsListEnded())
		{
			thisM = GetMonom();
			qM = q.GetMonom();
			if (*thisM == *qM)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
	return true;
}

int TPolinom::Calculate(int x, int y, int z)
{
	int res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += (int)(mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz));
		}
	}
	return res;
}

ostream & operator<<(ostream & os, TPolinom & q)
{
	if (q.ListLen)
	{
		PTMonom mon;
		int indx, indy, indz;
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			mon = q.GetMonom();
			indx = mon->GetIndex() / 100;
			indy = (mon->GetIndex() % 100) / 10;
			indz = mon->GetIndex() % 10;
			if (mon->GetCoeff() > 0)
				os << " + " << mon->GetCoeff();
			else
				os << " " << mon->GetCoeff();
			if (indx > 0)
				os << "x^" << indx;
			if (indy > 0)
				os << "y^" << indy;
			if (indz > 0)
				os << "z^" << indz;
		}
	}
	else
		os << 0;
	return os;
}
```

#### Тесты

##### Test_TDatList:

```c++
#include "DatList.h"
#include "Monom.h"
#include <gtest\gtest.h>

TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList l1);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList l1;
	ASSERT_TRUE(l1.IsEmpty());
}

TEST(TDatList, can_put_link_in_list)
{
	TDatList l1;
	PTMonom m1;
	m1 = new TMonom(4, 123);
	ASSERT_NO_THROW(l1.InsLast(m1));
}

TEST(TDatList, list_with_links_is_not_empty)
{
	TDatList l1;
	PTMonom m1, m2;
	m1 = new TMonom(4, 123);
	m2 = new TMonom(8, 456);
	l1.InsLast(m1);
	l1.InsLast(m2);
	ASSERT_TRUE((!l1.IsEmpty()) && (l1.GetListLength() == 2));
}

TEST(TDatList, can_get_current_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	EXPECT_EQ(l1.GetCurrentPos(), 0);
}

TEST(TDatList, can_set_current_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	EXPECT_EQ(l1.GetCurrentPos(), 4);
}

TEST(TDatList, can_go_next_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.GoNext();
	EXPECT_EQ(l1.GetCurrentPos(), 5);
}

TEST(TDatList, can_reset_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.Reset();
	EXPECT_EQ(l1.GetCurrentPos(), 0);
}

TEST(TDatList, ended_list_is_ended)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.GoNext();
	EXPECT_TRUE(l1.IsListEnded());
}

TEST(TDatList, can_get_link_from_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(pVal->GetIndex() + pVal->GetCoeff(), 560);
}

TEST(TDatList, can_put_link_before_the_first)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsFirst(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_after_the_last)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsLast(pVal);
	l1.SetCurrentPos(5);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_before_the_current)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(2);
	pVal = new TMonom(6, 666);
	l1.InsCurrent(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_delete_first_link)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	temp = (PTMonom)l1.GetDatValue();
	l1.DelFirst();
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((temp->GetIndex() + temp->GetCoeff() != pVal->GetIndex() + pVal->GetCoeff()) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_current_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(3);
	l1.DelCurrent();
	l1.SetCurrentPos(3);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((pVal->GetIndex() + pVal->GetCoeff() == 448) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.DelList();
	EXPECT_TRUE((l1.IsEmpty()) && (l1.GetListLength() == 0) && (l1.GetDatValue() == nullptr));
}
```
![](./image/testdl.jpg)

##### Test_TPolinom:

```c++
#include "Polinom.h"
#include <gtest\gtest.h>


TEST(TPolinom, Can_Make_Empty_Polinom)
{
	ASSERT_NO_THROW(TPolinom p);
}

TEST(TPolinom, Can_Make_Polinom_With_Arguments)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };

	ASSERT_NO_THROW(TPolinom p(monoms, 2));
}

TEST(TPolinom, Can_Copy_Polinom)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2);

	ASSERT_NO_THROW(TPolinom p2(p1));
}

TEST(TPolinom, Copied_Polinom_Is_Equal_To_Itself)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2(p1);

	EXPECT_EQ(true, p1 == p2);
}

TEST(TPolinom, Comparison_Returns_True_With_Equal_Polinoms)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2(monoms, 2);

	EXPECT_EQ(true, p1 == p2);
}

TEST(TPolinom, Comparison_Returns_False_With_Different_Polinoms)
{
	int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
	int monoms2[][2] = { { 6, 535 },{ 4, 207 } };
	TPolinom p1(monoms1, 2), p2(monoms2, 2);

	EXPECT_EQ(false, p1 == p2);
}

TEST(TPolinom, GetMonom_Works_Correctly)
{
	int monoms[][2] = { { 5, 701 } };
	TPolinom p(monoms, 1);
	TMonom m(5, 701);

	p.Reset();
	TMonom expected = *p.GetMonom();

	EXPECT_EQ(true, m == expected);
}

TEST(TPolinom, Can_Assign_Polinoms)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2;

	ASSERT_NO_THROW(p2 = p1);
}

TEST(TPolinom, Can_Assign_Polinom_To_Itself)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p(monoms, 2);

	ASSERT_NO_THROW(p = p);
}

TEST(TPolinom, Assign_Works_Correctly)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2;

	p2 = p1;

	EXPECT_EQ(true, p1 == p2);
}

TEST(TPolinom, Can_Add_Two_Polinoms)
{
	int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
	int monoms2[][2] = { { -2, 490 },{ 6, 027 } };
	int monoms3[][2] = { { 5, 701 },{ -2, 490 },{ 4, 207 },{ 6, 027 } };
	TPolinom p1(monoms1, 2), p2(monoms2, 2), p3(monoms3, 4);

	p1 = p1 + p2;

	ASSERT_TRUE(p3 == p1);
}

TEST(TPolinom, Can_Substract_Two_Polinoms)
{
	int monoms1[][2] = { { 5, 701 },{ 4, 207 }, {10, 027} };
	int monoms2[][2] = { { -2, 490 },{ 6, 027 } };
	int monoms3[][2] = { { 5, 701 },{ 2, 490 },{ 4, 207 },{ 4, 027 } };
	TPolinom p1(monoms1, 3), p2(monoms2, 2), p3(monoms3, 4);

	p1 = p1 - p2;

	ASSERT_TRUE(p3 == p1);
}

TEST(TPolinom, Can_Calculate_Polinom)
{
	int monoms[][2] = { { 5, 321 },{ -2, 301 } };
	TPolinom p(monoms, 2);

	EXPECT_EQ(2952, p.Calculate(2, 5, 3));
}

```
![](./image/testp.jpg)

#### Демонстрационная программа

```c++
#include "Polinom.h"
#include <iostream>
using namespace std;

void main()
{
	setlocale(LC_ALL, "Russian");

	int ms1[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	TPolinom poly1(ms1, 5);
	cout << "Полином 1:" << endl << poly1 << endl;

	int ms2[][2] = { { 2, 643 },{ 4, 431 },{ -8, 210 },{ 10, 50 } };
	TPolinom poly2(ms2, 4);
	cout << "Полином 2:" << endl << poly2 << endl;

	TPolinom sum_poly = poly1 + poly2;
	cout << "Сумма полиномов:" << endl << sum_poly << endl;

	TPolinom sub_poly = poly1 - poly2;
	cout << "Разность полиномов:" << endl << sub_poly << endl;

	int x, y, z;
	cout << "Введите значения переменных: ";
	cin >> x >> y >> z;
	cout << "При заданных x, y, z" << endl;
	cout << "Полином 1 равен: " << poly1.Calculate(x, y, z) << endl;
	cout << "Полином 2 равен: " << poly2.Calculate(x, y, z) << endl;
	cout << "Их сумма равна: " << sum_poly.Calculate(x, y, z) << endl;
	cout << "Их разность равна: " << sub_poly.Calculate(x, y, z) << endl;
}
```
![](./image/sample.jpg)

### Вывод

В ходе выполнения данной работы были получены навыки работы со структурой данных "список", а также разработано приложение для удобной работы с полиномами.