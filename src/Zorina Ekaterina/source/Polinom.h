#ifndef _POLINOM_H_
#define _POLINOM_H_

#pragma once
#include "HeadRing.h"
#include "Monom.h"

class TPolinom : public THeadRing 
{
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // �����������
												  // �������� �� ������� ������������-������
	TPolinom(TPolinom &q);      // ����������� �����������
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); };
	TPolinom & operator+(TPolinom &q); // �������� ���������
	TPolinom & operator-(TPolinom &q);	//��������� ���������
	TPolinom & operator=(TPolinom &q); // ������������
	bool operator==(TPolinom &q);	// ���������
	int Calculate(int x = 0, int y = 0, int z = 0);

	friend ostream & operator<<(ostream & os, TPolinom & q);
};
#endif
