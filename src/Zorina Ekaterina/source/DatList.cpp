#include "DatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

TDatList::TDatList()
{
	pFirst = nullptr;
	pLast = nullptr;
	pStop = nullptr;
	ListLen = 0;
	Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink temp;
	switch (mode)
	{
	case FIRST: temp = pFirst; break;
	case LAST: temp = pLast; break;
	default: temp = pCurrLink;  break;
	}
	return (temp == nullptr) ? nullptr : temp->pValue;
}

void TDatList::SetCurrentPos(int pos)
{
	if (pos < 0 || pos >= GetListLength())
		throw "Incorrect position index";
	Reset();
	for (int i = 0; i < pos; i++)
		GoNext();
}

int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}

void TDatList::Reset(void)
{
	pPrevLink = pStop;
	if (!IsEmpty())
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
	else
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
}

bool TDatList::IsListEnded(void) const
{
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{
	if (!IsListEnded())
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp == nullptr)
		throw "Incorrect DatLink pointer";
	pFirst = temp;
	ListLen++;
	if (ListLen == 1)
	{
		pLast = temp;
		Reset();
	}
	else
		if (CurrPos == 0)
			pCurrLink = temp;
		else
			CurrPos++;
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink temp = GetLink(pVal, pStop);
	if (temp == nullptr)
		throw "Incorrect DatLink pointer";
	if (pLast != nullptr)
		pLast->SetNextLink(temp);
	pLast = temp;
	ListLen++;
	if (ListLen == 1)
	{
		pFirst = temp;
		Reset();
	}
	if (pCurrLink == pStop)
		pCurrLink == temp;
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink temp = GetLink(pVal, pCurrLink);
		if (temp == nullptr)
			throw "Incorrect DatLink pointer";
		pPrevLink->SetNextLink(temp);
		temp->SetNextLink(pCurrLink);
		ListLen++;
		pCurrLink = temp;
	}
}

void TDatList::DelFirst(void)
{
	if (IsEmpty())
		throw "List is empty";
	PTDatLink temp = pFirst;
	pFirst = pFirst->GetNextDatLink();
	DelLink(temp);
	ListLen--;
	if (IsEmpty())
	{
		pLast = pStop;
		Reset();
	}
	else if (CurrPos == 0)
		pCurrLink = pFirst;
	else if (CurrPos == 1)
		pCurrLink == pStop;
	if (CurrPos)
		CurrPos--;
}

void TDatList::DelCurrent(void)
{
	if (pCurrLink != pStop)
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else
		{
			PTDatLink temp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(temp);
			ListLen--;

			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
}