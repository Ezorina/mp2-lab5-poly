#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (int i = 0; i < km; i++)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		monom = q.GetMonom();
		InsLast(monom->GetCopy());
	}
}

TPolinom & TPolinom::operator+(TPolinom & q)
{
	PTMonom qM, thisM, tempM;
	bool flag = true;
	TPolinom* res = new TPolinom();
	q.Reset();
	Reset();
	while (flag)
	{
		qM = q.GetMonom();
		thisM = GetMonom();
		if (qM->Index < thisM->Index)
		{
			res->InsLast(thisM->GetCopy());
			GoNext();
		}
		else if (thisM->Index < qM->Index)
		{
			res->InsLast(qM->GetCopy());
			q.GoNext();
		}
		else if (qM->Index == thisM->Index)
		{
			if (thisM->Index == -1)
				 flag = false;
			tempM = new TMonom(qM->Coeff + thisM->Coeff, thisM->Index);
			if (tempM->Coeff != 0)
				 res->InsLast(tempM->GetCopy());
			GoNext();
			q.GoNext();
		}
	}
	return *res;
}

TPolinom & TPolinom::operator-(TPolinom & q)
{
	PTMonom qM, thisM, tempM;
	bool flag = true;
	TPolinom* res = new TPolinom();
	q.Reset();
	Reset();
	while (flag)
	{
		qM = q.GetMonom();
		thisM = GetMonom();
		if (qM->Index < thisM->Index)
		{			
			res->InsLast(thisM->GetCopy());
			GoNext();
		}
		else if (thisM->Index < qM->Index)
		{
			tempM = new TMonom(-(qM->Coeff), qM->Index);
			res->InsLast(tempM->GetCopy());
			q.GoNext();
		}
		else if (qM->Index == thisM->Index)
		{
			if (thisM->Index == -1)
				flag = false;
			tempM = new TMonom( - qM->Coeff + thisM->Coeff, thisM->Index);
			if (tempM->Coeff != 0)
				res->InsLast(tempM->GetCopy());
			GoNext();
			q.GoNext();
		}
	}
	return *res;
}

TPolinom & TPolinom::operator=(TPolinom & q)
{
	if (&q != nullptr && &q != this)
	{
		DelList();
		PTMonom monom = new TMonom(0, -1);
		pHead->SetDatValue(monom);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			monom = q.GetMonom();
			InsLast(monom->GetCopy());
		}
	}
	return *this;
}

bool TPolinom::operator==(TPolinom & q)
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		Reset(); q.Reset();
		PTMonom thisM, qM;
		while (!IsListEnded())
		{
			thisM = GetMonom();
			qM = q.GetMonom();
			if (*thisM == *qM)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
	return true;
}

int TPolinom::Calculate(int x, int y, int z)
{
	int res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += (int)(mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz));
		}
	}
	return res;
}

ostream & operator<<(ostream & os, TPolinom & q)
{
	if (q.ListLen)
	{
		PTMonom mon;
		int indx, indy, indz;
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			mon = q.GetMonom();
			indx = mon->GetIndex() / 100;
			indy = (mon->GetIndex() % 100) / 10;
			indz = mon->GetIndex() % 10;
			if (mon->GetCoeff() > 0)
				os << " + " << mon->GetCoeff();
			else
				os << " " << mon->GetCoeff();
			if (indx > 0)
				os << "x^" << indx;
			if (indy > 0)
				os << "y^" << indy;
			if (indz > 0)
				os << "z^" << indz;
		}
	}
	else
		os << 0;
	return os;
}