#include "Polinom.h"
#include <gtest\gtest.h>


TEST(TPolinom, Can_Make_Empty_Polinom)
{
	ASSERT_NO_THROW(TPolinom p);
}

TEST(TPolinom, Can_Make_Polinom_With_Arguments)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };

	ASSERT_NO_THROW(TPolinom p(monoms, 2));
}

TEST(TPolinom, Can_Copy_Polinom)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2);

	ASSERT_NO_THROW(TPolinom p2(p1));
}

TEST(TPolinom, Copied_Polinom_Is_Equal_To_Itself)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2(p1);

	EXPECT_EQ(true, p1 == p2);
}

TEST(TPolinom, Comparison_Returns_True_With_Equal_Polinoms)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2(monoms, 2);

	EXPECT_EQ(true, p1 == p2);
}

TEST(TPolinom, Comparison_Returns_False_With_Different_Polinoms)
{
	int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
	int monoms2[][2] = { { 6, 535 },{ 4, 207 } };
	TPolinom p1(monoms1, 2), p2(monoms2, 2);

	EXPECT_EQ(false, p1 == p2);
}

TEST(TPolinom, GetMonom_Works_Correctly)
{
	int monoms[][2] = { { 5, 701 } };
	TPolinom p(monoms, 1);
	TMonom m(5, 701);

	p.Reset();
	TMonom expected = *p.GetMonom();

	EXPECT_EQ(true, m == expected);
}

TEST(TPolinom, Can_Assign_Polinoms)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2;

	ASSERT_NO_THROW(p2 = p1);
}

TEST(TPolinom, Can_Assign_Polinom_To_Itself)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p(monoms, 2);

	ASSERT_NO_THROW(p = p);
}

TEST(TPolinom, Assign_Works_Correctly)
{
	int monoms[][2] = { { 5, 701 },{ 4, 207 } };
	TPolinom p1(monoms, 2), p2;

	p2 = p1;

	EXPECT_EQ(true, p1 == p2);
}

TEST(TPolinom, Can_Add_Two_Polinoms)
{
	int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
	int monoms2[][2] = { { -2, 490 },{ 6, 027 } };
	int monoms3[][2] = { { 5, 701 },{ -2, 490 },{ 4, 207 },{ 6, 027 } };
	TPolinom p1(monoms1, 2), p2(monoms2, 2), p3(monoms3, 4);

	p1 = p1 + p2;

	ASSERT_TRUE(p3 == p1);
}

TEST(TPolinom, Can_Substract_Two_Polinoms)
{
	int monoms1[][2] = { { 5, 701 },{ 4, 207 }, {10, 027} };
	int monoms2[][2] = { { -2, 490 },{ 6, 027 } };
	int monoms3[][2] = { { 5, 701 },{ 2, 490 },{ 4, 207 },{ 4, 027 } };
	TPolinom p1(monoms1, 3), p2(monoms2, 2), p3(monoms3, 4);

	p1 = p1 - p2;

	ASSERT_TRUE(p3 == p1);
}

TEST(TPolinom, Can_Calculate_Polinom)
{
	int monoms[][2] = { { 5, 321 },{ -2, 301 } };
	TPolinom p(monoms, 2);

	EXPECT_EQ(2952, p.Calculate(2, 5, 3));
}
