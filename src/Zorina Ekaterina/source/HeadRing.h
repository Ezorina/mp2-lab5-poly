#ifndef _HEADRING_H_
#define _HEADRING_H_

#pragma once
#include "DatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // ���������, pFirst - ����� �� pHead
public:
	THeadRing();
	~THeadRing();
	// ������� �������
	virtual void InsFirst(PTDatValue pVal = NULL); // ����� ���������
												   // �������� �������
	virtual void DelFirst(void);                 // ������� ������ �����
};
#endif